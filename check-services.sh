#!/bin/bash

sudo apt install jq -y

STELLAR_URL="http://localhost:3000"

function log() {
	echo "$(date): $@"
}

function checkIfStellarNodeIsUpdating() {
	url=$1
	core_latest_ledger1=$(curl -s "$url" | jq '.core_latest_ledger')
	sleep 30s;
	core_latest_ledger2=$(curl -s "$url" | jq '.core_latest_ledger')
	if [[ "$core_latest_ledger1" == "$core_latest_ledger2" ]]; then
		log "DANGER! Stellar node is not updating"
		log "Resetting...."
		bash ./reset.sh
		log "Done reset."
	fi
}

log "Starting services..."
while true; do
	checkIfStellarNodeIsUpdating "$STELLAR_URL"
done
