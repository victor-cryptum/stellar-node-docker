#!/bin/bash
set -x
export $(cat .env | xargs)

rm -rf "$VOLUME_DIR/stellar-data/horizon-core-data/"
mkdir -p "$VOLUME_DIR/stellar-data/horizon-core-data"
docker rm -f $(docker ps -a -q)

docker run --rm -it \
    --env-file .env \
    -p "3000:8000" \
    -p "11626:11626" \
    -v "${VOLUME_DIR:-.}/stellar-data/horizon-core-data/horizon:/opt/stellar" \
    -v "${VOLUME_DIR:-.}/stellar-data/horizon-core-data/logs:/var/log/supervisor" \
    --name stellar stellar/quickstart
